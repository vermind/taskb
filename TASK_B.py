#!/usr/bin/env python
# coding: utf-8

# In[4]:


from concurrent.futures import ThreadPoolExecutor
import pandas as pd
from functools import reduce
import pandas as pd

# Map function, takes a row x of the DataFrame as input and returns a tuple where the first element is x and the second element is 1, 
#indicating that the row occurred once.
def map_func(x):
    
    return (x, 1)

# Shuffle function, takes the output of the map phase
#(a list of tuples) as input and groups the tuples by their key (the element from the DataFrame). Returns a dictionary where each key is associated with a list of values.
def shuffle(mapper_out):
   
    data = {}
    for k, v in mapper_out:
        if k not in data:
            data[k] = [v]
        else:
            data[k].append(v)
    return data

# Reduce function, takes two values x and y as input and returns their sum.
def reduce_func(x, y):
    
    return x + y

# Reduce function used to find the passenger(s) with the highest number of flights. Takes two values x and y as input,
#where x and y are tuples of the form (count, passenger_id). Returns the tuple with the higher count, or if the counts are equal, 
#returns a tuple with the sum of the counts and the passenger_id.
def max_reduce_func(x, y):
    
    if x[0] > y[0]:
        return x
    elif x[0] < y[0]:
        return y
    else:
        return (x[0], x[1] + y[1])
    
# Function that removes duplicate rows from the DataFrame based on column pairs. Returns the modified DataFrame.
def remove_duplicates(df, col1_name, col2_name, col3_name):
    
    mask1 = df.duplicated(subset=[col1_name, col2_name], keep=False)
    mask2 = df.duplicated(subset=[col1_name, col3_name], keep=False)
    mask = mask1 | mask2

    if mask.any():
        print("There are duplicate rows in the dataset based on column pairs.")
        duplicated_rows = df[mask]
        df.drop_duplicates(subset=[col1_name, col2_name], inplace=True)
        df.drop_duplicates(subset=[col1_name, col3_name], inplace=True)
        print(f"{len(duplicated_rows)} duplicate rows removed.")
        return df
    else:
        print("There are no duplicate rows in the dataset based on column pairs.")
        return df
    
df2 = pd.read_csv("/Users/dxy/Desktop/UOR/bigdata/taskb/AComp_Passenger_data_no_error_DateTime(1).csv", header=None)
df2.head()

if __name__ == '__main__':
    # Load data
    df = remove_duplicates(df2,0,5,7)
    col1 = df.columns[0]
    map_in = df[col1].tolist()

    # Map phase, apply the map function to each element of the list using a ThreadPoolExecutor.
    with ThreadPoolExecutor() as executor:
        map_out = list(executor.map(map_func, map_in))
   # Shuffle phase, group the tuples by their key and return a dictionary where each key is associated with a list of values. # Shuffle phase
    reduce_in = shuffle(map_out)

    # Reduce phase, apply the reduce function to each list of values in the dictionary using a ThreadPoolExecutor.
    #The result is a dictionary where each key is associated with a tuple of the form (count, passenger_id).
    with ThreadPoolExecutor() as executor:
        futures = {}
        reduce_out = {}
        for key, values in reduce_in.items():
            futures[key] = executor.submit(reduce, reduce_func, values)
        for key, future in futures.items():
            reduce_out[key] = future.result()

    # Find passenger(s) with the highest number of flights
    max_count = max(reduce_out.values())
    max_passengers = [k for k, v in reduce_out.items() if v == max_count]
    print(f"The passenger(s) with the highest number of flights is/are: {max_passengers} (count: {max_count})")


# In[ ]:




